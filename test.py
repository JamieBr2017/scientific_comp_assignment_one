from BVP import solution, ddt,T_period

import numpy as np
import matplotlib.pyplot as plt
import math
import random

print()

#random variables show the flexibility of the code 
X0 = np.random.rand(2)+0.2
par = random.randint(1,20)/10
t0 = 0 
t1 = 60
h = 1e-3
tspan = np.linspace(t0,t1,int(t1/h))
sols = solution(X0,par,t0,t1,h,tspan)
time_period = T_period(sols[:,0],tspan,t1)
t = random.randint(1000,len(sols))

print('X0 =',X0, ',par =',par, ',t =',round(t*1e-3,3), '\n')

# print(sols[t], [math.sqrt(par)*np.sin(t*1e-3), math.sqrt(par)*np.cos(t*1e-3)], '\n')

print('Test for periodic boundary conditions:')
diff_vec = (abs(sols[0,:] - sols[int(time_period/1e-3),:]))
print(diff_vec)
if all(i < 1e-3 for i in diff_vec) == True:
	print("Test successful\n")
else:
	print("Test failed\n")
# supercritical Hopf bifurcation has an explicit solution in cos and sin
print('Test against known solutions:')
cos_vec = abs(abs(sols[t,1]) - abs(math.sqrt(par)*np.cos(t*1e-3)))
print(cos_vec)
print('u1: ', end = '')
if cos_vec < 1e-3:
	print("Test successful\n", end = '')
else:
	print("Test failed\n")

sin_vec = abs(abs(sols[t,0]) - abs(math.sqrt(par)*np.sin(t*1e-3)))
print(sin_vec)
print('u2: ', end = '')
if sin_vec < 1e-3:
	print("Test successful\n")
else:
	print("Test failed\n")

