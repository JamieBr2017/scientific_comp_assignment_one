import numpy as np
import math
import scipy
import sys
import pandas as pd
from scipy.optimize import fsolve 
from scipy.integrate import odeint
from pylab import *
	
def ddt(X,t,b,t0,t1,h,tspan):
	"""
		A function where the user should write their differential equations.
		The function takes the state of the system and unpacks it into the respective state variables.
		The differential terms are then calculated for the specific state of the system. This is vectorized and returned 
		as the array U.
		
		User Input
		----------
		1). Equate len(x) to the number of differential equations in the system.
		2). Unpack the state of the system by equating  X to the state variables with commas separating them 
			for example:	
				x,y = X
		3). Write the differential equations of the system
		4). Equate U to an np.array of the differential equations. 
			for example:
				U = np.array([x_dot,y_dot])
										
		Parameters
		----------
		The user defines parameters they require for thier respective ode.

		
		Returns 
		-----------
		array containing the value of the differential equations at a specific time.
		
		
		Additional Information
		-----------
		There is a system of ODE's already input as an example.
	"""
	if len(X) ==2:
		x,y = X
		x_dot = b*x - y  -x*(x**2 + y**2)
		y_dot = x + b*y  -y*(x**2 + y**2) 
		U = np.array([x_dot,y_dot])
	else:
		raise Exception('\n Dimensions of initial conditions does not match those of the ODEs')
	return U

	
def values(X0,par,t0,t1,h,tspan):
	"""
		Function which uses odeint to numerically integrate the system of ODEs input by the user
	"""
	sols= odeint(ddt,X0,tspan,args = (par,t0,t1,h,tspan))
	return sols
	
	
def troughs(x):
	"""
		Function which finds the minima in the system of ODEs
	"""
	minima = np.where((x[1:-1]<x[0:-2])*(x[1:-1] < x[2:]))[0] + 1;
	return minima

	
def T_period(x,tspan,t1):
	"""
		Function which takes the last two minima returned from function "troughs" to calculate
		the time period of the system
	"""
	dips = troughs(x)
	time_x = tspan[dips]
	if len(time_x)>=2:
		time_period = time_x[-1] - time_x[-2]
	else:
		raise TypeError('\n Attempted ODE is non-periodic')
	return time_period 

	
def display(X0,par,t0,t1,h,tspan):
	"""
		Function which returns the time periods of the differential equations as a list
	"""

	sols = values(X0,par,t0,t1,h,tspan)
	periodTs = []
	for i in range(sols.shape[1]):
		periodT = T_period(sols[:,i],tspan,t1)
		periodTs.append(periodT)	
	return [*periodTs]

	
def phase_conditioning(X0,ddt,par,t0,t1,h,tspan):
	"""
		Function which sets a phase condition for the initial conditions to satisfy.
		The current phase condition written within this function is y_dot.
		y_dot is returned and run through the inbuilt function fsolve
		in the next function "solution". Therefore the overall phase condition is that y_dot = 0
		
		
		User input
		----------
		If the user wishes the change the phase condition they should comment out the y_dot line 
		and change it to the phase condition they require. 
	
	"""
	periodT = display(X0,par,t0,t1,h,tspan)[0]
	tspan = np.linspace(0,periodT,int(periodT/h))
	fun = odeint(ddt,X0,tspan,args=(par,t0,t1,h,tspan))[-1,1] - X0[1]
	y_dot = ddt(X0,tspan,par,t0,t1,h,tspan)[1]	
	return [fun,y_dot]

	
def solution(X0,par,t0,t1,h,tspan):
	"""
		Function runs "phase_conditioning" through fsolve to return the correct initial conditions
		for a given parameter value.
		If a set parameter value is given "solution" also plots a graph and 
		prints time periods, the guessed initial conditions and the correct initial conditions to the terminal.
	
	"""
	par = float(par)
	periodT = display(X0,par,t0,t1,h,tspan)[0]
	X0_values = fsolve(phase_conditioning, X0,args = (ddt,par,t0,t1,h,tspan))#,xtol = 1e-5)
	sols = values(X0_values,par,t0,t1,h,tspan)
	if len(sys.argv) == 1:
		return sols
	if len(sys.argv)==2:
		for i in range(values(X0,par,t0,t1,h,tspan).shape[1]):
			print("Period T (X",i+1,") = ",display(X0,par,t0,t1,h,tspan)[i])
		print("Initial X = ", X0, "\nSolved X0 = ", sols[0])
		fig,ax = plt.subplots()
		for i in range(sols.shape[1]):
			plot(tspan,sols[:,i])
		ax.set_xlabel("Time Elapsed")
		ax.set_ylabel(" State Variables")
		plt.show()
		
		
def param_cont(X0,par,t0,t1,h,tspan):
	"""
		Function runs "solution" over the range of parameter values inputed by the user. Returns maxima and minima of state
		variables and the corresponding parameter value.
	"""
	X0_plot = np.zeros((len(par),2))
	X0_final = X0	
	for i in range(len(par)):
		b = (par[i])
		try:
			p = solution(X0_final,b,t0,t1,h,tspan)
		except TypeError:
			return par,X0_plot
		X0_final = p[0,:]		
		X0_plot[i,:] = [np.amin(p[:]),np.amax(p[:])]
	return par, X0_plot
			
	
def running(X0, par , t0 , t1, h,tspan):
	"""
		Function  which runs either "solution" or "param_cont" depending on whether a set parameter value is given. 

		Inputs
		---------
	
		X0: guess at initial conditions of state variables
		par: range of parameter values of interest
		t0: initial time
		t1: final time
		h:  step size for numerical integration. for example: 1e-3
		tspan: range of times with step size. 
	
		
	"""
	if len(sys.argv) ==2:	
		solution(X0,sys.argv[1],t0,t1,h,tspan)
	else:
		par, X0_plot = param_cont(X0,par,t0,t1,h,tspan)
		fig,ax = plt.subplots()
		plt.plot(par,X0_plot)
		ax.set_xlabel("Parameter Value")
		ax.set_ylabel("Maximum and Minimum of State Variables")
		plt.show()
		


"""
	Run the function "running" below. Refer to the documentation of the function "running" for 
	required inputs. An example is written below
"""
running([0.35,0.35],np.linspace(2,-1,100),0,100,1e-3,np.linspace(0,40,int(40/1e-3)))

